import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router';
import axios from 'axios';
import * as filters from './util/filter'
import ElementUI from 'element-ui';
import store from "./store";

Object.keys(filters).forEach(k => Vue.filter(k, filters[k]));//注册过滤器

// axios.defaults.timeout = 30000;
// axios.defaults.baseURL = 'http://192.168.67.112:8888/';

Vue.use(VueRouter);
Vue.use(ElementUI);

import Login from "./components/login";
import Index from "./components/index";

var routes = [
  {path: '/login', name: "login", component: Login},
  {path: "/", component: Index}
];
const router = new VueRouter({
  routes
});

router.beforeEach((to, from, next) => {
  next();
});

new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store
});
